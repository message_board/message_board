<?php
    function dbConnect(){
        $db_type = 'mysql';
        $db_host = 'localhost';
        $db_name = 'message_board';
        $db_user = 'root';
        $db_password = '';
        $dbconnect = 'mysql:host=' . $db_host . ";dbname=" . $db_name;
        $db = new PDO($dbconnect, $db_user, $db_password);
        $db->query("SET NAMES UTF8");
        return $db; 
    }
?>