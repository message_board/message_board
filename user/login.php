<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>登入</title>
    <link rel="stylesheet" type="text/css" href="">
    <style>
        body {
            background-image: url("/message_board/img/bg.png");
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
            background-size: cover;
        }

        .title {
            font-size: 60px;
            position: absolute;
            top: -100px;
            left: 145px;
        }

        #login_frame {
            width: 400px;
            height: 320px;
            padding: 13px;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -200px;
            margin-top: -200px;
            background-color: #ACA6A2;
            border-radius: 15px;
            text-align: center;
        }

        form p>* {
            display: inline-block;
            vertical-align: middle;
        }

        .text_field {
            width: 278px;
            height: 28px;
            border-radius: 10px;
            border: 0;
        }

        .text_name1 {
            width: 190px;
            height: 20px;
        }

        .text_name2 {
            width: 150px;
            height: 20px;
        }

        .text_name3 {
            width: 130px;
            height: 20px;
        }

        #btn_login {
            font-size: 16px;
            width: 80px;
            height: 28px;
            line-height: 14px;
            text-align: center;
            color: #343434;
            background-color: #B7CCED;
            border-radius: 14px;
            border: 0;
            float: right;
            position: absolute;
            top: 270px;
            left: 260px;
        }

        #register1 {
            position: relative;
            top: 25px;
            left: -65px;
        }

        #register {
            font-size: 16px;
            color: #1A5AFF;
            text-decoration: none;
            position: relative;
            top: 25px;
            left: -65px;
        }

        #register:hover {
            color: #AF0101;
            text-decoration: underline;
        }

        #login_control {
            padding: 0 28px;
        }

        /* .button{
            color: black;
            background-color: #ADD8E6;
            border-color: #ADD8E6;
        } */
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src=""></script>
</head>

<body>
    <!-- <nav class="navbar bg-dark navbar-dark ">
        <a class="navbar-brand"><img src="../MESSAGE_BOARD/star.png"></a>
        <ul class="nav justify-content-end">
        <li class="nav-item">
            <a class="nav-link" href="#">登入</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">註冊</a>
        </li>
        </ul>
    </nav> -->
    <div id="login_frame">
        <h1 class="title"><b>登入</b></h1>
        <form method="post" action=" /message_board/user/login.php">
            <p>
            <h5 class="text_name1"><b>使用者名稱</b></h5>
            </p>
            <input class="text_field" type="text" name="name" require placeholder="輸入帳號">
            <p>
            <h5 class="text_name2"><b>Email</b></h5>
            </p>
            <input class="text_field" type="email" name="email" require placeholder="輸入常用信箱">
            <p>
            <h5 class="text_name3"><b>密碼</b></h5>
            </p>
            <input class="text_field" type="password" name="password" placeholder="輸入密碼">
            <div id="login_control">
                <input type="hidden" name="id" value="id">
                <input type="submit" name="submit" id="btn_login" value="登入" onclick="login();" />
                <a id="register1">尚未註冊？</a>
                <a id="register" src="">馬上註冊</a>
            </div>
        </form>
    </div>
    <!-- <script>
        function login() {
            var name = document.getElementById("name");
            ar pass = document.getElementById("password");
            if (name.value == "") {
                alert("請輸入使用者名稱");
            } else if (pass.value  == "") {
                alert("請輸入密碼");
            } else if(name.value == "admin" && pass.value == "123456"){
                window.location.href="welcome.html";
            } else {
                alert("請輸入正確的使用者名稱和密碼！")
            }
        }
    </script> -->
</body>

</html>